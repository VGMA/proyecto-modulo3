import { Component, OnInit } from '@angular/core';
import {DestinosApiClient} from  './../models/destinos-api-client.model';
import {DestinoViaje} from  './../models/destino-viaje.model';
import {ActivatedRoute} from '@angular/router';
import {AppState} from 'src/app/app.model';
import {Store} from '@ngrx/store';

class DestinosApiClientViejo{
  getById(id: String): DestinoViaje{
    console.log('llama por la clave vieja')
    return null;
  }
}
interface Appconfig{
  apiEndpoint: String;
}
const APP_CONFIG_VALUE: Appconfig = {
  apiEndpoint:mi_api.com
};
const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

class DestinosApiClientDecorated extends DestinosApiClient {
  constructor(@Inject(APP_CONFIG) private config: AppConfig, store: Store<AppState>){
    super(store);
  }
}
getById(id: String): DestinoViaje {
  console.log('llamado por la clase');
  console.log('config: ' + this.config.apliEndpoint);
  return super.getById(id);
}

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [
    DestinosApiClient,
    {privide: APP_CONFIG, useValue: APP_CONFIG_VALUE},
    {privade: DestinosApiClient, useClass: DestinosApiClientDecorated},
    {provide: DestinosApiClientViejo, useExisting: DestinosApiClient}
  ]
})
export class DestinoDetalleComponent implements OnInit {
	destino:DestinoViaje;

  constructor(private route: ActivatedRoute, private destinosApiClient: DestinosApiClientViejo) { }

  ngOnInit(): void {
  	const id=this.route.snapshot.paramMap.get('id');
  	this.destino=null;
  	//this.destinosApiClient.getById(id);

  }

}
