import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// importar modulo de ruteo
import  {RouterModule, Routes} from '@angular/router';

import  { FormsModule, ReactiveFormsModule} from '@angular/forms';
import  { StoreModule as NgRxStoreModule, ActionReducerMap} from '@ngrx/store' ;
import  { EffectsModule } from '@ngrx/effects' ;
import { StoreDevtoolsModule} from '@ngrx/store-devtools'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './destino-detalle/destino-detalle.component';
import { FormDestinoViajeComponent } from './form-destino-viaje/form-destino-viaje.component';
import { DestinosApiClient } from './models/destinos-api-client.model';
import { DestinosViajesState,
        reducerDestinosViajes,
        intializeDestinosViajesState,
         DestinosViajesEffects
         } from './models/destinos-viajes-state.model';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { AuthService } from './services/auth.service';
import { VuelosComponentComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { VuelosMainComponentComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosDetalleComponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { ReservasModule } from './reservas/reservas.module';



export const childrenRoutesVuelos: Routes= [
  {path: '', redirectTo:'main', pathMatch:'full'},
  {path: 'main', component: VuelosMainComponentComponent},
  {path: 'mas-info', component: VuelosMasInfoComponentComponent},
  {path: ':id', component: VuelosDetalleComponent},
];

const routes: Routes=[
{path:'', redirectTo:'home', pathMatch:'full'},
{path:'home', component: ListaDestinosComponent},
{path:'destino/:id', component: DestinoDetalleComponent},
{path: 'login', component: LoginComponent},
{
  path: 'protected',
  component: ProtectedComponent,
  canActivate: [ UsuarioLogueadoGuard ]
},
{
  path: 'vuelos',
  component: VuelosComponentComponent,
  canActivate: [ UsuarioLogueadoGuard],
  children: childrenRoutesVuelos
}
];
//redux init
//estado global de la app
export interface AppState{
  destinos: DestinosViajesState;
}
// reducers globales de la app
const reducers: ActionReducerMap<AppState>={
  destinos: reducerDestinosViajes
};
// inicializacion
const reducersInitialState={
  destinos: intializeDestinosViajesState()
}
//redux fin init


@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentComponent,
    VuelosMainComponentComponent,
    VuelosMasInfoComponentComponent,
    VuelosDetalleComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers, {initialState: reducersInitialState,
      runtimeChecks:{
        strictStateImmutability:false,
        strictActionImmutability:false,
      }
    }),
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument(),
    ReservasModule
  ],


  providers: [
  AuthService, UsuarioLogueadoGuard
 ],
  bootstrap: [AppComponent]
})

export class AppModule { }
